from django.db import models
import uuid


class Alias(models.Model):
    name = models.CharField(max_length=255)


class Person(models.Model):
    id = models.UUIDField(default=uuid.uuid4, db_index=True, unique=True, primary_key=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    aliases = models.ManyToManyField(Alias)
    movies_as_actor = models.ManyToManyField("movies.Movie")
    movies_as_director = models.ManyToManyField("movies.Movie", related_name="director_people")
    movies_as_producer = models.ManyToManyField("movies.Movie", related_name="producer_people")

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"
