from django.urls import path

from apps.movies.viewsets.movie_viewset import MovieViewSet
from apps.movies.viewsets.person_viewset import PersonViewSet

app_name = "movasdf"

urlpatterns = [
    path("movies/", MovieViewSet.as_view({"get": "list", "post": "create"}), name="movies_list"),
    path("movies/<id>/",
         MovieViewSet.as_view({"get": "retrieve", "post": "create", "patch": "partial_update", "delete": "destroy"}),
         name="movie_details"),
    path("movies/<id>/enable/", MovieViewSet.as_view({"patch": "enable"}), name="movie_enable"),
    path("movies/<id>/disable/", MovieViewSet.as_view({"patch": "disable"}), name="movie_disable"),
    path("people/", PersonViewSet.as_view({"get": "list", "post": "create"}), name="people_list"),
    path("people/<id>/", PersonViewSet.as_view({"get": "retrieve", "patch": "partial_update", "delete": "destroy"}),
         name="people_details")
]
