class PersonValidationMixin:
    def _validate_short_person_response(self, data, person):
        self.assertIsInstance(data, dict)
        self.assertEqual(data.pop("id", None), str(person.id))
        self.assertEqual(data.pop("full_name", None), person.full_name)
        self.assertTrue(str(person.id) in data.pop("_link", ""))
        self.assertFalse(data)

    def _validate_person_response(self, data, person):
        self.assertIsInstance(data, dict)
        self.assertEqual(data.pop("id", None), str(person.id))
        self.assertEqual(data.pop("first_name", None), person.first_name)
        self.assertEqual(data.pop("last_name", None), person.last_name)

        for alias_data, alias in zip(data.pop("aliases", []), person.aliases.all()):
            self.assertEqual(alias_data["name"], alias.name)

        for movie_data, movie in zip(data.pop("movies_as_actor", []), person.movies_as_actor.all()):
            self._validate_short_movie_response(movie_data, movie)

        for movie_data, movie in zip(data.pop("movies_as_director", []), person.movies_as_director.all()):
            self._validate_short_movie_response(movie_data, movie)

        for movie_data, movie in zip(data.pop("movies_as_producer", []), person.movies_as_producer.all()):
            self._validate_short_movie_response(movie_data, movie)

        self.assertFalse(data)


class MovieValidationMixin:
    def _validate_short_movie_response(self, data, movie):
        self.assertIsInstance(data, dict)
        self.assertEqual(data.pop("id", None), str(movie.id))
        self.assertEqual(data.pop("title", None), movie.title)
        self.assertTrue(str(movie.id) in data.pop("_link", ""))
        self.assertFalse(data)

    def _validate_movie_response(self, data, movie):
        self.assertIsInstance(data, dict)
        self.assertEqual(data.pop("id", None), str(movie.id))
        self.assertEqual(data.pop("title", None), movie.title)
        self.assertEqual(data.pop("genre", None), movie.genre.name if movie.genre else None)
        self.assertEqual(data.pop("release_year", None), int(movie.release_year))
        self.assertEqual(data.pop("is_active", None), movie.is_active)
        for casting_data, person in zip(data.pop("casting", []), movie.casting.all()):
            self._validate_short_person_response(casting_data, person)

        for directors_data, person in zip(data.pop("directors", []), movie.directors.all()):
            self._validate_short_person_response(directors_data, person)

        for producers_data, person in zip(data.pop("producers", []), movie.producers.all()):
            self._validate_short_person_response(producers_data, person)
        self.assertFalse(data)


class PaginatedResponseValidationMixin:
    def _validate_paginated_response(self, response, object_list, validator):
        self.assertEqual(response.json()["count"], len(object_list))
        results = response.json()["results"]

        for data, obj in zip(results, object_list):
            validator(data, obj)
