from apps.movies.tests.factories.movie_factory import MovieFactory
from apps.movies.tests.factories.person_factory import PersonFactory

if __name__ == "__main__":
    PersonFactory.create_batch(3)
    MovieFactory.create_batch(3)
