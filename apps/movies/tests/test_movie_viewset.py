from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from apps.movies.tests.factories.movie_factory import MovieFactory
from apps.movies.tests.validators import PersonValidationMixin, MovieValidationMixin, PaginatedResponseValidationMixin


class MoviesListViewSetTest(TestCase, PersonValidationMixin, MovieValidationMixin, PaginatedResponseValidationMixin):
    url = reverse("movies:movies_list")

    def test_access(self):
        pass

    def test_list(self):
        movies = MovieFactory.create_batch(3)

        with self.assertNumQueries(5):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        self._validate_paginated_response(response, movies, self._validate_movie_response)
