from rest_framework import serializers
from rest_framework.reverse import reverse

from apps.movies.models import Person, Movie, Alias


class ShortMovieSerializer(serializers.ModelSerializer):
    _link = serializers.SerializerMethodField(read_only=True)

    def get__link(self, obj):
        return reverse("movies:movie_details", kwargs={"id": str(obj.id)})

    class Meta:
        model = Movie
        fields = ("id", "title", "_link")


class AliasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alias
        fields = ("name",)


class PersonSerializer(serializers.ModelSerializer):
    aliases = AliasSerializer(many=True, read_only=True)
    movies_as_actor = ShortMovieSerializer(many=True, read_only=True)
    movies_as_director = ShortMovieSerializer(many=True, read_only=True)
    movies_as_producer = ShortMovieSerializer(many=True, read_only=True)

    class Meta:
        model = Person
        fields = (
            "id",
            "first_name",
            "last_name",
            "aliases",
            "movies_as_actor",
            "movies_as_director",
            "movies_as_producer"
        )
