from django.core.management.base import BaseCommand, CommandError

from apps.movies.tests.factories.movie_factory import MovieFactory
from apps.movies.tests.factories.person_factory import PersonFactory


class Command(BaseCommand):
    help = "Create fake data."

    def handle(self, *args, **options):
        people = PersonFactory.create_batch(4)
        movies = MovieFactory.create_batch(4)

        for person in people:
            person.movies_as_actor.add(movies[0])
            person.movies_as_director.add(movies[1])
            person.movies_as_producer.add(movies[0])

        self.stdout.write(self.style.SUCCESS("Successfully seed data added."))
