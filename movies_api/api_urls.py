from django.urls import path, include

urlpatterns = [
    path("", include("apps.movies.urls", namespace="movies")),
]
