import factory
from apps.movies.models import Movie
from apps.movies.tests.factories.film_genre_factory import FilmGenreFactory
from apps.movies.tests.factories.person_factory import PersonFactory


class MovieFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: f"title-{n}")
    release_year = factory.Faker("year")
    genre = factory.SubFactory(FilmGenreFactory)

    class Meta:
        model = Movie

    @factory.post_generation
    def people(obj, create, _):
        if not create:
            return
        person = PersonFactory()
        obj.casting.add(person)
        obj.directors.add(person)
        obj.producers.add(person)
