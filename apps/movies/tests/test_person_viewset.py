from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from apps.movies.models import Person
from apps.movies.tests.factories.movie_factory import MovieFactory
from apps.movies.tests.factories.person_factory import PersonFactory
from apps.movies.tests.validators import PersonValidationMixin, MovieValidationMixin, PaginatedResponseValidationMixin


class PersonViewSetTest(TestCase, PersonValidationMixin, MovieValidationMixin, PaginatedResponseValidationMixin):
    path = reverse("movies:people_list")

    def setUp(self):
        self.people = PersonFactory.create_batch(3)
        movie = MovieFactory()

        for person in self.people:
            person.movies_as_actor.add(movie)
            person.movies_as_director.add(movie)
            person.movies_as_producer.add(movie)
        self.people = Person.objects.all()
        self.user = User.objects.create_user("test", "test@gmail.com", "password")
        self.client.force_login(self.user)

    def test_access(self):
        self.client.logout()
        response = self.client.get(self.path)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = {"first_name": "fake", "last_name": "fake2"}

        response = self.client.post(self.path, data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        person = PersonFactory()
        path = reverse("movies:people_details", kwargs={"id": str(person.id)})
        response = self.client.patch(path, data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.delete(path, data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.client.force_login(self.user)

        response = self.client.post(self.path, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.patch(path, data=data, content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.delete(path, data=data)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_list(self):
        response = self.client.get(self.path)
        self.assertTrue(response.status_code, status.HTTP_200_OK)
        self._validate_paginated_response(response, self.people, self._validate_person_response)

    def test_create(self):
        data = {"first_name": "fake", "last_name": "fake2"}
        response = self.client.post(self.path, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Person.objects.filter(**data).exists())

    def test_patch(self):
        data = {"first_name": "other_name", "last_name": "fake2"}
        person = PersonFactory()
        path = reverse("movies:people_details", kwargs={"id": str(person.id)})
        response = self.client.patch(path, data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
