FROM python:3

ENV PYTHONBUFFERED 1

RUN mkdir src

COPY ./ ./movies_api

WORKDIR movies_api

RUN pip install -r requirements.txt