import factory

from apps.movies.models import FilmGenre


class FilmGenreFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: f"genre-{n}")

    class Meta:
        model = FilmGenre
