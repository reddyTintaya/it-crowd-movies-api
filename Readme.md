## Movies Company 


## Requirements
You need to have docker and docker compose installed

## Steps to run the container
1. Setup container
```commandline
docker compose up
```
The project by its self will add some fake data and a superuser with credentials:

```
username: admin
password: passw0rd
```

2. Runserver

Recommended, so you can see the logs for the celery tasks
```commandline
docker compose up
```

or if you need to add a breakpoint in the code
```commandline
docker-compose run --service-ports --rm web python ./manage.py runserver 0.0.0.0:8000
```

## Run the Tests
```commandline
docker-compose run --rm web pytest apps
```


## Endpoints
Postman collection can be found at:
IT Crowd.postman_collection.json
or 
https://www.getpostman.com/collections/026e3c0e3a9641d4f6f5

### People endpoints
- `localhost:8000/api/v1/people/`
  - List, create
- `localhost:8000/api/v1/people/<id>/`
  - retrieve, patch, delete 

### Movie endpoints
- `localhost:8000/api/v1/movies/`
    - List, create
    - filterable by `is_active` param `?is_active=false`
- `localhost:8000/api/v1/movies/<id>/`
    - retrieve, patch, delete 
- `localhost:8000/api/v1/movies/<id>/enable/`
  - PATCH
  - Changes the `is_active` field in the Movie record and notifies to a fake url asynchronously
- `localhost:8000/api/v1/movies/<id>/disable/`
  - PATCH
  - Changes the `is_active` field to false



## Project tech stack

- `Django Rest Framework` as the framework to build the Web API
- `Celery` & `Redis` for the tasks
- `Postgres SQL` as the DB
- `pytest` as the testing tool 
- For authentication, it uses Basic Authentication with username and password