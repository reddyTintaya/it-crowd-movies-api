from rest_framework import serializers
from rest_framework.reverse import reverse

from apps.movies.models import Movie, Person


class ShortPersonSerializer(serializers.ModelSerializer):
    _link = serializers.SerializerMethodField(read_only=True)

    def get__link(self, obj):
        return reverse("movies:people_details", kwargs={"id": str(obj.id)})

    class Meta:
        model = Person
        fields = ("id", "full_name", "_link")


class MovieSerializer(serializers.ModelSerializer):
    genre = serializers.CharField(source="genre.name", default=None)
    casting = ShortPersonSerializer(many=True, read_only=True)
    directors = ShortPersonSerializer(many=True, read_only=True)
    producers = ShortPersonSerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        fields = ("id", "title", "genre", "release_year", "casting", "directors", "producers", "is_active")
