from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, SAFE_METHODS, IsAuthenticated
from rest_framework.response import Response

from apps.movies.models import Movie, FilmGenre
from apps.movies.serializers.movie_serializer import MovieSerializer
from apps.movies.tasks import report_status_change_task


class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    permission_classes = [AllowAny]
    lookup_field = "id"
    lookup_url_kwarg = "id"

    def get_queryset(self):
        qs = super().get_queryset()

        is_active = self.request.query_params.get("is_active")
        if is_active is not None:
            qs = qs.filter(is_active=is_active in ["1", 1, True, "true"])

        return qs.select_related("genre"). \
            prefetch_related("casting", "directors", "producers", "genre")

    def get_permissions(self):
        if self.request.method not in SAFE_METHODS:
            self.permission_classes = (IsAuthenticated,)
        return super().get_permissions()

    @property
    def movie(self):
        return self.get_object()

    def _set_movie_availability(self, is_active=True):
        self.movie.is_active = is_active
        self.movie.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=["PATCH"])
    def enable(self, request, *args, **kwargs):
        report_status_change_task.delay()
        return self._set_movie_availability()

    @action(detail=True, methods=["PATCH"])
    def disable(self, request, *args, **kwargs):
        return self._set_movie_availability(is_active=False)

    def perform_create(self, serializer):
        if "genre" in self.request.data:
            genre, _ = FilmGenre.objects.get_or_create(name=self.request.data["genre"])
        serializer.save(genre=genre)