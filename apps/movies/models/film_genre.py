from django.db import models
import uuid


class FilmGenre(models.Model):
    id = models.UUIDField(default=uuid.uuid4, db_index=True, unique=True, primary_key=True)
    name = models.CharField(max_length=255)
    movies = models.ManyToManyField("Movie")
