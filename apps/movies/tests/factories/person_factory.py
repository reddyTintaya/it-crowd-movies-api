import factory

from apps.movies.models import Person, Alias


class AliasFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: f"alias-{n}")
    class Meta:
        model = Alias


class PersonFactory(factory.django.DjangoModelFactory):
    last_name = factory.Faker("last_name")
    first_name = factory.Faker("first_name")

    class Meta:
        model = Person

    @factory.post_generation
    def aliases(obj, create, _):
        if not create:
            return
        aliases = AliasFactory.create_batch(3)
        for alias in aliases:
            obj.aliases.add(alias)
