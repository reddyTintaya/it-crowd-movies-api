from django.db import models
import uuid


class Movie(models.Model):
    id = models.UUIDField(default=uuid.uuid4, db_index=True, unique=True, primary_key=True)
    title = models.CharField(max_length=255)
    genre = models.ForeignKey("movies.FilmGenre", on_delete=models.PROTECT, null=True, blank=True)
    release_year = models.PositiveIntegerField()
    casting = models.ManyToManyField("movies.Person", related_name="cast_at")
    directors = models.ManyToManyField("movies.Person", related_name="director_at")
    producers = models.ManyToManyField("movies.Person", related_name="producer_at")
    is_active = models.BooleanField(default=True)
